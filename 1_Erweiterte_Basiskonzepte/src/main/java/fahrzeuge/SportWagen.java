package fahrzeuge;

public class SportWagen extends PKW {
    public boolean nebellampen;
    public int kurvenlicht = 0;

    public SportWagen(String farbe) {
        super(farbe);
        System.out.println("Konstruktor von: " + SportWagen.class + " mit farbe: " + farbe + " aufgerufen");
    }

    @Override
    public void lichtAn() {
        System.out.println("Kurven Lampen an bei " + kurvenlicht + " Grad.");
        this.licht = true;
        this.kurvenlicht = 0;
    }

    @Override
    public boolean hatAutopilot(){
        return true;
    }

    @Override
    public void fahren() {
        System.out.println("Der " + farbe + "e Sportwagen flitzt!");
    }

    @Override
    public void fuehreServiceDurch() {
        System.out.println("Dauert 2 Tage (viele Zündkerzen");
    }

    public void nebellampenAn() {
        System.out.println("Nebellampen an");
        this.nebellampen = true;
    }

}
