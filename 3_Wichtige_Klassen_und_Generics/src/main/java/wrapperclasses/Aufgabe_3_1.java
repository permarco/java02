package wrapperclasses;

import java.util.Arrays;

public class Aufgabe_3_1 {

    private final static String HILFE =
            "Anwendung: java Berechne <zahl1> +|-|x|/ <zahl2>";

    public static void main (String[] args) {

        // Stimmt die Zahl der Argumente?
        if (args.length != 3) {
            System.out.println(HILFE);
            return;
        }

        // Ist das erste Argument gueltig?
        double z1;
        try {
            z1 = Double.parseDouble(args[0]);
        }
        catch(NumberFormatException e) {
            System.out.println("zahl1 ungueltig");
            return;
        }

        // Ist das zweite Argument gueltig?
        args[1] = args[1].trim();
        if (!Arrays.asList(new String[]{"+","-","x","/"}).contains(args[1])) {
            System.out.println("operator ungueltig: " + args[1]);
            System.out.println(HILFE);
            return;
        }

        // Ist das dritte Argument gueltig?
        double z2;
        try {
            z2 = Double.parseDouble(args[2]);
        }
        catch(NumberFormatException e) {
            System.out.println("zahl2 ungueltig");
            return;
        }

        // Fuehre die Operation aus
        switch(args[1].charAt(0)) {
            case '+': System.out.println(z1 + z2);break;
            case '-': System.out.println(z1 - z2);break;
            case 'x': System.out.println(z1 * z2);break;
            case '/': System.out.println(z1 / z2);break;
        }

    }
}


