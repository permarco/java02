package comparable;

interface StopWatch {
    long getTimeInMillis();
}
