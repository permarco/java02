package firstexceptions.exception_und_vererbung;

public interface InterfaceKlasse {

    void methode1(boolean shouldThrowException) throws UnterException;

    void methode2(boolean shouldThrowException) throws UnterException;

    void methode3(boolean shouldThrowException) throws OberException;

    void methode4(boolean shouldThrowException) throws UnterException;
}
