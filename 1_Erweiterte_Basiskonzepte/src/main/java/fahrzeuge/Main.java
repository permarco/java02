package fahrzeuge;

public class Main {

    public static void main(String[] args) {
        creationTest();
//        instaceofTest();
//        polymorphTest();
//        innereKlasseTest();
//        anonymeKlasseTest();
    }

    public static void creationTest() {
        System.out.println("***********CreationTest Start*******************");
        PKW pkw = new SportWagen("rot");
        Fahrzeug fahrzeug = pkw; // casting to Fahrzeug
        fahrzeug.fahren();
        fahrzeug.farbe = "weiss";
        pkw.lichtAn();
        SportWagen sportWagen = (SportWagen) pkw; // casting back to SportWagen
        sportWagen.nebellampenAn();
    }

    public static void instaceofTest() {
        System.out.println("***********InstanceofTest Start*******************");
        Fahrzeug fahrzeug = new SportWagen("rot");
        boolean isSportWagen = fahrzeug instanceof SportWagen;
        boolean istFahrzeug = fahrzeug instanceof Fahrzeug;

        System.out.println(fahrzeug.getClass());
        System.out.println("ist ein Sportwagen: " + isSportWagen);
        System.out.println("ist ein Fahrzeug: " + istFahrzeug);
        fahrzeug.fahren();
//        fahrzeug.nebellampenAn();  // geht nicht man muss explizit casten
        SportWagen sportWagen1 = (SportWagen) fahrzeug;
        sportWagen1.nebellampenAn();

        if (fahrzeug instanceof SportWagen lokalerSportWagen) { // neu in Java 17 cast mit neuer Variable direkt in den instaceof
            System.out.println("Sind Nebellampen an: " + lokalerSportWagen.nebellampen);
            lokalerSportWagen.nebellampenAn();
            System.out.println("Sind Nebellampen an: " + lokalerSportWagen.nebellampen);
        }

        System.out.println("Bei null gibt instance of immer : " + (null instanceof SportWagen) + " zurück.");

//        boolean istAnderesObjekt = fahrzeug instanceof AnderesObjekt; // geht nicht weil Fahrzeug nicht von AnderesObjekt erbt
    }

    public static void polymorphTest() {
        System.out.println("***********PolymorphTest Start*******************");
        Fahrzeug pkw = new PKW("rot");
        Fahrzeug sportWagen = new SportWagen("gelb");

        executeFahren(pkw);
        executeFahren(sportWagen);

    }

    private static void executeFahren(Fahrzeug fahrzeug) {
        fahrzeug.fahren();
    }

    public static void innereKlasseTest() {
        System.out.println("***********innereKlasseTest Start*******************");
        AnderesObjekt anderesObjekt = new AnderesObjekt();
        AnderesObjekt.InnereKlasse innereKlasse = anderesObjekt.new InnereKlasse();
        System.out.println(anderesObjekt.at);
        System.out.println(AnderesObjekt.sat);
        System.out.println(innereKlasse.it);
        System.out.println(AnderesObjekt.InnereKlasse.sit);
        innereKlasse.print();
    }

    public static void anonymeKlasseTest() {
        System.out.println("***********anonymeKlasseTest Start*******************");

        // anonyme Klasse mit sofortigem Aufruf
        new Fahrzeug() {
            @Override
            void fahren() {
                System.out.println("Dieser Zug fährt von Zürich nach Bern.");
            }
        }.fahren();

        // anonyme Klasse mit Namen (zug). Es ist aber dennoch anony weil es dazu keine Klasse gibt.
        Fahrzeug zug = new Fahrzeug() {
            @Override
            void fahren() {
                System.out.println("Dieser Zug fährt von Zürich nach Bern.");
            }
        };
        zug.fahren();
    }
}
