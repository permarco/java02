package firstexceptions;

/**
 * Zweites Besispiel:
 *
 * Eine Division durch 0 verurscaht eine implizite Exception.
 */
public class SecondException {

    public static void main(String[] args) {
        System.out.println(4 / 0);
    }

}
