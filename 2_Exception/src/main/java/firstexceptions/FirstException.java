package firstexceptions;

/**
 * Erstes Beispiel
 *
 * Exceptions können explizit geworfen werden. Das wird mit "throw new" geamcht.
 */
public class FirstException {

    public static double sqrt(double d) throws Exception {
        if (d < 0) {
            throw new Exception("Parameter negativ!");
        }
        return Math.sqrt(d);
    }

    public static void main(String[] args) {
        System.out.println("start program");
        try {
            System.out.println(sqrt(25));
            sqrt(-25);
            System.out.println("wird nicht ausgeführt");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        System.out.println("end program");
    }
}
