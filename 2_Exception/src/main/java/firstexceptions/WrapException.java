package firstexceptions;

/**
 * WrapException zweigt wie man eine fremde Exception in eine benutzerdefinierte Exception umwandelt.
 */
public class WrapException {

    public static double unsaveSqrt(double d) throws MyException{
        try {
            if (d < 0) {
                throw new Exception("Parameter negativ!");
            }
            return Math.sqrt(d);
        } catch (Exception ex) {
            // Die exception wird in eine benutzerdefinierte Exception umgewandelt
            throw new MyException(ex.getMessage(), 999);
        }
    }

    public static void main(String[] args) {

        // unsaveSqrt wirft eine checked (erbt von Exception) benutzerdefinierte Exception
        try {
            unsaveSqrt(-25);
        } catch (MyException mex) {
            System.out.println(mex.getClass().getName() + mex.toString());
        }

    }
}
