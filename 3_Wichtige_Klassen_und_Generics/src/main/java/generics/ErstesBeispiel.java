package generics;

public class ErstesBeispiel {


    public static void main(String[] args) {
        useStringStack();
        useIntegerStack();
        useObjectStack();
        useGenericStack();
    }

    private static void useGenericStack() {
        System.out.println("=> start use Generics");
        GenericStack<String> myGenericStack = new GenericStack<String>(12);
        myGenericStack.push("string");
        String s = myGenericStack.pop();
        System.out.println(s);
//        myGenericStack.push(Integer.valueOf(1)); // Ein Integer kann nicht in den Stack gespeichert werden.
//        s = (String) myGenericStack.pop();
//        System.out.println(s);
        System.out.println("=> stop use Generics");
    }

    private static void useObjectStack() {
        System.out.println("=> start use Objects");
        ObjectStack myObejctStack = new ObjectStack(12);
        myObejctStack.push("string");
        String s = (String) myObejctStack.pop();
        System.out.println(s);
        myObejctStack.push(Integer.valueOf(1));
        Object o = myObejctStack.pop();
        System.out.println(o);
        System.out.println(o instanceof Integer); // Das zurückgegeben Objekt ist kein String sondern ein Integer
//        s = (String) o; // Kann nicht in String gecastet werden

        System.out.println(myObejctStack);
        System.out.println("=> stop use Objects");
    }

    private static void useIntegerStack() {
        System.out.println("=> start use Integer");
        IntegerStack myIntegerStack = new IntegerStack(12);
        myIntegerStack.push(1);
        myIntegerStack.push(2);
        myIntegerStack.push(3);
        myIntegerStack.push(4);

        System.out.println(myIntegerStack);
        System.out.println("=> stop use Integer");
    }

    private static void useStringStack() {
        System.out.println("=> start use String");
        StringStack myStringStack = new StringStack(12);
        myStringStack.push("halo");
        myStringStack.push("Java");
        myStringStack.push("is");
        myStringStack.push("cool");

        System.out.println(myStringStack);
        System.out.println("=> stop use String");
    }
}
