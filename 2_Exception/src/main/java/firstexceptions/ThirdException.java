package firstexceptions;

/**
 * Drittes Beispiel
 *
 * Exceptions können innerhalb der gleichen Methode behandelt werden oder können an den Aufurfer weitergegeben werden.
 */
public class ThirdException {

    public static double saveSqrt(double d) {
        try {
            if (d < 0) {
                throw new Exception("Parameter negativ!");
            }
            return Math.sqrt(d);
        } catch (Exception ex) {
            // exception wird in der gleichen Methode behandelt
            System.out.println(ex.getClass().getName() + " Message: " + ex.getMessage());
        }
        return 0.0; // ein default-Wert wird zurückgegeben
    }

    // Eine exception kann an den Aufrufer weitergereicht werden. Dies wird mit "throws" in der Signatur gekennzeichnert.
    public static double unsaveSqrt(double d) throws Exception {
        if (d < 0) {
            throw new Exception("Parameter negativ!");
        }
        return Math.sqrt(d);
    }

    public static void main(String[] args) {

        // saveSqrt wirft keine Exception diese wird innerhalb der Methode saveSqrt behandlet
        saveSqrt(-25);

        // unsaveSqrt wirft eine checked Exception welche abgefagen werden muss
        try {
            unsaveSqrt(-25);
        } catch (Exception ex) {
            System.out.println(ex.getClass().getName() + " Message: " + ex.getMessage());
        }

    }
}
