package random;

import java.util.Random;

public class useRandom {
    public static void main(String[] args) {

        Random random1 = new Random(); // nicht reproduzierbar

        Random random2 = new Random(3l); // reproduzierbar

        System.out.println(random1.nextInt());
        System.out.println(random1.nextBoolean());
        System.out.println(random1.nextDouble());
        System.out.println(random1.nextFloat());
        System.out.println(random1.nextLong());

        System.out.println(random1.nextGaussian()); // Zufallszahl zwischen 0.0 und 1.0

    }
}
