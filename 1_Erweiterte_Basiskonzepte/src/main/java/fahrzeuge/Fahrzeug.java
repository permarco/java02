package fahrzeuge;

public abstract class Fahrzeug {

    protected String farbe;

    public Fahrzeug() {
        System.out.println("DefaultKonstruktor von: " + Fahrzeug.class + " (abstract) aufgerufen");
        this.farbe = "Grundfahrbe";
    }

    abstract void fahren();

}
