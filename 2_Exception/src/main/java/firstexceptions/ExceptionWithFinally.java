package firstexceptions;

/**
 * Der finally-Block kann nach jedem try-Block hizugefügt werden.
 * Dieser finally-Block wird in jedem Fall ausgeführt.
 */
public class ExceptionWithFinally {

    public static void calculate() {
        System.out.println("Programm Start");
        try {
            if (Math.random() > 0.5) {
                System.out.println(2 / 0);
            } else {
                System.out.println(2 / 2);
            }
        } catch (ArithmeticException ae) {
            System.out.println(ae.getClass().getName() + " Message: " + ae.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getClass().getName() + " Message: " + ex.getMessage());
        } finally {
            System.out.println("finally wird in jedem Fall ausgeführt");
        }
        System.out.println("Programm Ende");
    }

    public static void main(String[] args) {
        calculate();
    }
}
