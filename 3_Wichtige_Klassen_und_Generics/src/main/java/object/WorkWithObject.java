package object;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Parameter;

public class WorkWithObject {

    public static void main(String[] args) {
        useClone();
        useToString();
        useEquals();
        useHashCode();
        usegetClass();
    }

    private static void useClone() {
        System.out.println("=> start use clone");
        MyObject obj = new MyObject("Mein Objekt");
        // Der Pointer zeigt auf ein anders (neues) Objekt
        MyObject kopie = obj.clone();
        System.out.println("Ist es das gleiche Objekt: " + (kopie == obj));
        System.out.println("Sind die Objekte vom gleichen Typ: " + (obj.clone().getClass() == obj.getClass()));
        System.out.println("Haben die Objekte den gleichen Namen: " + kopie.objectName.equals(obj.objectName));
        System.out.println("=> stop use clone");
        System.out.println(obj.getClass().getConstructors()[1]);
    }

    private static void useToString() {
        System.out.println("=> start use toString");
        MyObject obj = new MyObject("mein Objekt");
        System.out.println(obj);
        System.out.println("=> stop use toString");
    }

    private static void useEquals() {
        System.out.println("=> start use equals");
        MyObject obj1 = new MyObject("objekt");
        MyObject obj2 = obj1; // zeigt auf das gleiche Objekt
        MyObject obj3 = new MyObject("anderes Objekt");
        MyObject obj4 = new MyObject("objekt");
        MyObject obj5 = obj1.clone();

        System.out.println("obj1=?obj2 (referenzgleich) : " + obj1.equals(obj2));
        System.out.println("obj1=?obj3 (verschieden): " + obj1.equals(obj3));
        System.out.println("obj1=?obj4 (gleich): " + obj1.equals(obj4));

        System.out.println("obj1=?obj5 (clone): " + obj5.equals(obj1));

        System.out.println("=> start use equals best practice");

        // Der Vergleich eines Objekt mit sich selbst gibt true zurück
        System.out.println("Der Vergleich eines Objekt mit sich selbst gibt true zurück: " + obj1.equals(obj1));

        // Ist x gleich y, dann ist y auch gleich x.
        System.out.println("Ist x gleich y, dann ist y auch gleich x: " + (obj1.equals(obj4) == obj4.equals(obj5)));

        // Ist x gleich y und ist y gleich z, dann ist x auch gleich z.
        System.out.println("Ist x gleich y und ist y gleich z, dann ist x auch gleich z: " + (obj1.equals(obj4) == obj4.equals(obj5) == obj1.equals(obj5)));

        // Solange sich zwei Objekte nicht verändern, gibt equals immer dasselbe Resultat zurück.
        System.out.println("Solange sich zwei Objekte nicht verändern, gibt equals immer dasselbe Resultat zurück: " + obj1.equals(obj4));
        System.out.println("Solange sich zwei Objekte nicht verändern, gibt equals immer dasselbe Resultat zurück: " + obj1.equals(obj4));
        obj4.objectName = "neuer Name";
        System.out.println("Solange sich zwei Objekte nicht verändern, gibt equals immer dasselbe Resultat zurück: " + obj1.equals(obj4));

        // Vergleicht man ein Objekt mit null, ist das Ergebnis immer false
        System.out.println("Vergleicht man ein Objekt mit null, ist das Ergebnis immer false" + obj1.equals(null));

        System.out.println("=> stop use equals");

    }

    private static void useHashCode() {
        System.out.println("=> start use hschcode");

        MyObject obj1 = new MyObject("objekt");
        MyObject obj2 = new MyObject("anderes Objekt");
        MyObject obj3 = new MyObject("objekt");
        MyObject obj4 = new MyObject(null);

        System.out.println("hashcode von obj1: " + obj1.hashCode());
        System.out.println("hashcode von obj2: " + obj2.hashCode());
        System.out.println("hashcode von obj3: " + obj3.hashCode());
        System.out.println("hashcode von obj4: " + obj4.hashCode());

        System.out.println("=> stop use hschcode");
    }


    private static void usegetClass() {
        System.out.println("=> start use getClass");
        MyObject object = new MyObject("my reflection");
        System.out.println(object.getClass().getSimpleName());
        System.out.println(object.getClass().getName());
        Constructor<?>[] constructors = object.getClass().getConstructors();
        for (Constructor c : constructors) {
            String message = "Constructor " + c.getName() + " has " + c.getParameterCount() + " parameter";
            for (Parameter p : c.getParameters()) {
                message += " " + p.getName() + " " + p.getType().getName();
            }
            System.out.println(message);
        }

        MyObject object2 = new MyObject("my reflection");
        Class clazz = object2.getClass();
        try {
            Field[] declaredFields = clazz.getDeclaredFields();
            for (Field f : declaredFields) {
                f.setAccessible(true);  // macht das auch mit privaten Felder interagiert werden kann.
                System.out.println(f.getName() + " = " + f.get(object2));
                f.set(object2, "geschrieben über Reflections");
            }
            System.out.println(object2.objectName);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        System.out.println("=> stop use getClass");
    }
}
