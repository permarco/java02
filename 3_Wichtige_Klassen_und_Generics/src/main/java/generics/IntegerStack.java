package generics;

public class IntegerStack {

    private Integer[] stack;
    private int pos;

    IntegerStack(int size) {
        stack = new Integer[size];
        pos = 0;
    }

    public void push(Integer number) {
        stack[pos++] = number;
    }

    public Integer pop() {
        return stack[--pos];
    }

    @Override
    public String toString() {
        String output = "";
        for (int i = 0; stack.length > i; i++) {
            output += "[" + stack[i] + "]";
        }
        return output;
    }
}
