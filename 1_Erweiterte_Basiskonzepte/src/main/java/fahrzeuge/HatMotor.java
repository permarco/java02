package fahrzeuge;

public interface HatMotor {

    void betaetigeGaspedal();

    void fuehreServiceDurch();

    default boolean hatAutopilot() {
        return false;
    }
}
