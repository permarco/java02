package firstexceptions;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * MultipleCatch zeigt wie man mehrere, nicht hierarchische in Beziehung stehende Exception gleich behandelt.
 */
public class MultipleCatch {

    public static void multipleException() throws IOException, TimeoutException {
        // code here
    }

    public static void main(String[] args) {
        try {
            multipleException();
        } catch (IOException | TimeoutException e) { // | steht für "oder"
            e.printStackTrace();
        }
    }
}
