package system;

import java.io.IOException;
import java.io.PrintStream;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;

public class WorkingWithSystem {

    public static void main(String[] args) throws IOException {

        long StartTime = System.currentTimeMillis();
        System.out.println(LocalDateTime.ofInstant(Instant.ofEpochMilli(StartTime), ZoneId.systemDefault()));
        useProperties();
        useEnv();
        useIO();
        long StopTime = System.currentTimeMillis();
        System.out.println("Program dauerte: " + (StopTime - StartTime) / 1000 + " Sekunden");
    }

    private static void useEnv() {
        System.out.println("--ENV--");
        System.out.println(System.getenv("PATH"));

        Map<String, String> getenv = System.getenv();
        for (String key : getenv.keySet()) {
            System.out.println("Key: " + key + ", Value: " + System.getenv(key));
        }
        System.out.println("--ENV--");
    }

    private static void useProperties() {
        System.out.println("--Properties--");
        System.out.println(System.getProperty("java.specification.version"));
        System.out.println(System.getProperty("myproperty.system", "prod"));
        System.setProperty("myproperty.system", "test");
        System.out.println(System.getProperty("myproperty.system", "prod"));
        System.clearProperty("myproperty.system");
        System.out.println(System.getProperty("myproperty.system", "prod"));

        Properties properties = System.getProperties();
        Enumeration propertyNames = properties.propertyNames();
        while (propertyNames.hasMoreElements()) {
            String PropertyName = (String) propertyNames.nextElement();
            System.out.println(PropertyName + "=" + System.getProperty(PropertyName));
        }
        System.out.println("--Properties--");
    }

    private static void useIO() throws IOException {

        // Schreibe in standart output
        System.out.println("Some output to console 1");

        // lese Eingabe
        System.out.println("input your name: ");
        byte[] name = new byte[12];
        System.in.read(name, 0, 12);
        System.out.println(new String(name));

        // Schreibe error
        System.err.println("some inline error message");

        // change output to File
        String filename = "myNewLog.log";
        System.out.println("The Logfile is found in: " + System.getProperty("user.dir") + System.getProperty("file.separator") + filename);
        System.setOut(new PrintStream(filename));
        System.out.println("Some output in Logfile");
    }
}
