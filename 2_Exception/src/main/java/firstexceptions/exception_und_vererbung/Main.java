package firstexceptions.exception_und_vererbung;

public class Main {

    public static void main(String[] args) {

        OberKlasse oberKlasse = new OberKlasse();
        UnterKlasse unterKlasse = new UnterKlasse();

        try{
            executeAllFunctions(oberKlasse);
            executeAllFunctions(unterKlasse);
        } catch (OberException oe){
            System.out.println(oe.getClass().getName() + " Message: " + oe.getMessage());
        }
    }

    public static void executeAllFunctions(InterfaceKlasse klasse) throws OberException {
        klasse.methode1(true);
        klasse.methode2(true);
        klasse.methode3(true);
        klasse.methode4(true);
    }
}
