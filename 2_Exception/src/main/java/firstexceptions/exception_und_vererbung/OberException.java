package firstexceptions.exception_und_vererbung;

public class OberException extends Exception{

    public OberException(String message) {
        super(message);
    }
}
