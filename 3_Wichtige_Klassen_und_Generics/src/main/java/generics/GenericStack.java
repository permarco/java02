package generics;

public class GenericStack<E> {

    private E[] stack;
    private int pos;

    GenericStack(int size) {
        stack = (E[]) new Object[size];
        pos = 0;
    }

    public void push(E e) {
        stack[pos++] = e;
    }

    public E pop() {
        return stack[--pos];
    }

    @Override
    public String toString() {
        String output = "";
        for (int i = 0; stack.length > i; i++) {
            output += "[" + stack[i] + "]";
        }
        return output;
    }
}
