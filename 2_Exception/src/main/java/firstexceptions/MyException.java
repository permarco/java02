package firstexceptions;

/**
 * MyException ist eine benutzerdefinierte Exception
 */
public class MyException extends Exception {

    private int reasoncode;

    MyException() {
        super("unknown Error");
        this.reasoncode = -1;
    }

    MyException(String message, int reasoncode) {
        super(message);
        this.reasoncode = reasoncode;
    }

    @Override
    public String toString() {
        return "Message: " + getMessage() + " Errorcode: " + reasoncode;
    }
}
