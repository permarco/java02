package wrapperclasses;

public class WrapperClasses {

    public static void main(String[] args) {
        theProblem();
        boxedObjectsInArray();
        booleanCreation();
        charCreation();
        byteCreation();
        shortCreation();
        integerCreation();
        longCreation();
        floatCreation();
        doubleCreation();
        minMax();
    }

    private static void theProblem() {
        System.out.println("=> start the problem");
        char myChar = 'A';
        char[] chars = new char[2];
        chars[0] = myChar;
        boolean myBool = false;
//        chars[1] = myBool; // es ist nicht mögliche einen boolean (primitiven Datentyp) in einem Array von Typ char (primitiven Datentyp) zuzuweisen
        System.out.println("=> stop the problem");
    }


    private static void boxedObjectsInArray() {
        System.out.println("=> start Boxed Objectd in Array");
        Object[] objects = new Object[5];
        objects[0] = new Integer(2);
        objects[1] = new Character('Q');
        objects[2] = new Long(2_000);
        objects[3] = new Boolean(false);
        objects[4] = new Double(2.333);
        System.out.println("<= stop Boxed Objectd in Array");

        System.out.println(objects[2].toString());
    }


    private static void booleanCreation() {
        System.out.println("=> start bool creation");
        boolean myBool = false;

        // boxing
        Boolean myBoxedBool1 = new Boolean(myBool);
        Boolean myBoxedBool2 = Boolean.valueOf(true);

        // unboxing
        boolean myUnboxedBool = myBoxedBool1.booleanValue();
        System.out.println(myBool == myUnboxedBool);
        System.out.println("<= stop bool creation");
    }

    private static void charCreation() {
        System.out.println("=> start char creation");
        char myChar = 'A';

        // boxing
        Character myBoxedCharacter1 = new Character(myChar);
        Character myBoxedCharacter2 = Character.valueOf('A');
        Character myBoxedCharacter3 = Character.valueOf('A');
        System.out.println(myBoxedCharacter1 == myBoxedCharacter2);
        System.out.println(myBoxedCharacter1.equals(myBoxedCharacter2));
        System.out.println(myBoxedCharacter1.compareTo(myBoxedCharacter2));
        System.out.println(myBoxedCharacter2 == myBoxedCharacter3);

        // unboxing
        char myUnboxedCahr = myBoxedCharacter1.charValue();
        System.out.println(myChar == myUnboxedCahr);
        System.out.println("<= stop char creation");
    }


    private static void byteCreation() {
        System.out.println("=> start byte creation");
        byte myByte = 1;

        // boxing
        Byte myBoxedByte1 = new Byte(myByte);
        Byte myBoxedByte2 = new Byte("1");
        Byte myBoxedByte3 = Byte.valueOf(myByte);

        // unboxing
        byte myUnboxedByte = myBoxedByte1.byteValue();
        System.out.println(myByte == myUnboxedByte);
        System.out.println("=> stop byte creation");

    }

    private static void shortCreation() {
        System.out.println("=> start short creation");
        short myShort = 2;

        // boxing
        Short myBoxedShort1 = new Short(myShort);
        Short myBoxedShort2 = Short.valueOf(myShort);

        // unboxing
        short myUnboxedShort = myBoxedShort1.shortValue();
        System.out.println(myShort == myUnboxedShort);
        System.out.println("=> stop short creation");

    }

    private static void integerCreation() {
        int myInt = 33;

        // boxing
        Integer myBoxedInt1 = new Integer(myInt);
        Integer myBoxedInt2 = Integer.valueOf(myInt);

        int i = myBoxedInt1 + myBoxedInt2;

        // unboxing
        int myUnboxedInt = myBoxedInt1.intValue();
        System.out.println(myInt == myUnboxedInt);

        System.out.println("=> start int creation");
        System.out.println("=> stop int creation");
    }

    private static void longCreation() {
        System.out.println("=> start long creation");
        long myLong = 33;

        // boxing
        Long myBoxedLong1 = new Long(myLong);
        Long myBoxedLong2 = Long.valueOf(myLong);

        // unboxing
        long myUnboxedLong = myBoxedLong1.longValue();
        System.out.println(myLong == myUnboxedLong);

        System.out.println("=> stop long creation");

    }

    private static void floatCreation() {
        System.out.println("=> start float creation");
        float myFloat = 33.33f;

        // boxing
        Float myBoxedFloat1 = new Float(myFloat);
        Float myBoxedFloat2 = Float.valueOf(myFloat);

        // unboxing
        float myUnboxedFloat = myBoxedFloat1.floatValue();
        System.out.println(myFloat == myUnboxedFloat);
        System.out.println("=> stop float creation");
    }

    private static void doubleCreation() {
        System.out.println("=> start double creation");
        double myDouble = 33.33d;

        // boxing
        Double myBoxedDouble1 = new Double(myDouble);
        Double myBoxedDouble2 = Double.valueOf(myDouble);

        // unboxing
        double myUnboxedDouble = myBoxedDouble1.doubleValue();
        System.out.println(myDouble == myUnboxedDouble);
        System.out.println("=> stop double creation");
    }

    private static void minMax(){
        System.out.println("=> start minmax");
        System.out.println(Byte.MIN_VALUE);
        System.out.println(Byte.MAX_VALUE);
        System.out.println("=> stop minmax");
    }
}
