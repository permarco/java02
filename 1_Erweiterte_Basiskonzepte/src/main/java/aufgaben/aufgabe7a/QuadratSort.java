package aufgaben.aufgabe7a;

import java.util.Arrays;
import java.util.Comparator;

public class QuadratSort {

    public static void main(String[] args) {
        Quadrat[] einArray = {
                new Quadrat(23),
                new Quadrat(10),
                new Quadrat(33)
        };

        // order
        Arrays.sort(einArray, new Comparator() {
            public int compare(Object o1, Object o2) {
                return 0;
            }
        });

        // print
        for (int i = 0; einArray.length > i; i++) {
            System.out.println(einArray[i]);
        }
    }

    private static class Quadrat implements Comparable{
        protected int seitenlaenge = 0;

        public Quadrat(int seitenlaenge) {
            this.seitenlaenge = seitenlaenge;
        }

        @Override
        public String toString() {
            return "Quadrat mit der Fläche: " + (seitenlaenge * seitenlaenge);
        }

        @Override
        public int compareTo(Object o) {
            Quadrat other = (Quadrat) o;
            return this.seitenlaenge - other.seitenlaenge;
        }
    }
}
