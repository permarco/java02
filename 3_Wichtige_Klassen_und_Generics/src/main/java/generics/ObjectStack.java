package generics;

public class ObjectStack {

    private Object[] stack;
    private int pos;

    ObjectStack(int size) {
        stack = new Object[size];
        pos = 0;
    }

    public void push(Object object) {
        stack[pos++] = object;
    }

    public Object pop() {
        return stack[--pos];
    }

    @Override
    public String toString() {
        String output = "";
        for (int i = 0; stack.length > i; i++) {
            output += "[" + stack[i] + "]";
        }
        return output;
    }
}
