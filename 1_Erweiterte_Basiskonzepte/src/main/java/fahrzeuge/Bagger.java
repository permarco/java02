package fahrzeuge;

public class Bagger extends Fahrzeug implements HatMotor {

    public static final String FAHREN = "FAHREN";
    public static final String BAGGERN = "BAGGERN";

    public String status = FAHREN;

    @Override
    public void fahren() {
        System.out.println("Der " + farbe + "e Bagger rollt mit Raupe");
    }

    @Override
    public void betaetigeGaspedal() {
        if (status.equalsIgnoreCase(FAHREN)) {
            fahren(); // oder this.fahren();
        } else if (status.equalsIgnoreCase(BAGGERN)) {
            System.out.println("Hebe die Schaufel");
        }
    }

    @Override
    public void fuehreServiceDurch() {
        System.out.println("Dauert 3 Tage");
    }

    public void umschaltung() {
        if (status.equalsIgnoreCase(FAHREN)) {
            status = BAGGERN;
        } else {
            status = FAHREN;
        }
    }
}
