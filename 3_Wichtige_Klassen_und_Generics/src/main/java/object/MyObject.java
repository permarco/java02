package object;

public class MyObject implements Cloneable { // erweitert implizit von Object

    String objectName;
    private String privateName = "private";

    public MyObject() {
        this.objectName = "unknown";
    }

    public MyObject(String objectName) {
        this.objectName = objectName;
    }

    @Override
    public MyObject clone() {
        return new MyObject(this.objectName);
    }

    @Override
    public String toString() {
        return "Das Objekt ist von Typ: " + this.getClass().getSimpleName() + " und heisst: " + this.objectName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyObject myObject = (MyObject) o;

        return objectName != null ? objectName.equals(myObject.objectName) : myObject.objectName == null;
    }

    @Override
    public int hashCode() {
        return objectName != null ? objectName.hashCode() : 0;
    }

}
