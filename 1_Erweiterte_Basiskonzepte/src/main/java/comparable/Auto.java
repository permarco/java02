package comparable;

import static java.util.Arrays.sort;

public class Auto implements Comparable {

    public int erstzulassung;

    public Auto(int erstzulassung) {
        this.erstzulassung = erstzulassung;
    }

    @Override
    public String toString() {
        return "erstzulassung: " + erstzulassung;
    }

    @Override
    public int compareTo(Object other) {
        Auto andersAuto = (Auto) other;
        return this.erstzulassung - andersAuto.erstzulassung;
    }

    public static void main(String[] args) {
        Auto[] autos = new Auto[4];
        autos[0] = new Auto(2022);
        autos[1] = new Auto(1965);
        autos[2] = new Auto(2001);
        autos[3] = new Auto(1995);
        System.out.println("===================> vor dem Sortieren");
        print(autos);
        sort(autos);
        System.out.println("===================> nach dem Sortieren");
        print(autos);
    }

    private static void print(Auto[] autos) {
        for (Auto auto : autos) {
            System.out.println(auto);
        }
    }
}
