package fahrzeuge;

public class AnderesObjekt {
    static String sat = "static anderer text";
    String at = "anderer text";

    class InnereKlasse {
        static String sit = "static inner text";
        String it = "inner text";
        public void print() {
            System.out.println(at);
            System.out.println(sat);
            System.out.println(it);
            System.out.println(sit);
        }
    }
}
