package firstexceptions.exception_und_vererbung;

public class UnterException extends OberException {

    public UnterException(String message) {
        super(message);
    }
}

