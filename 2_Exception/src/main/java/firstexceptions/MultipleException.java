package firstexceptions;

/**
 * MultipleException zeigt wie man mehrere Exception der gleichen hiearchiestruktur separat auffängt.
 */
public class MultipleException {

    public static int[] packInArray(int d) {
        int[] myArray = new int[1];
        myArray[1] = d;
        return myArray;
    }

    private static void firstStep() {
        System.out.println("first step:");
        try {
            packInArray(25);
//        } catch (Exception ex) { // Die Exception Hierarchie muss beim multy-catch berücksichtigt werden.
//            System.out.println(ex.getClass().getName() + " Message: " + ex.getMessage());
        } catch (ArrayIndexOutOfBoundsException aioobe) {
            System.out.println(aioobe.getClass().getName() + " Message: " + aioobe.getMessage());
        } catch (IndexOutOfBoundsException ioobe) {
            System.out.println(ioobe.getClass().getName() + " Message: " + ioobe.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getClass().getName() + " Message: " + ex.getMessage());
        }
    }

    private static void secondStep() {
        System.out.println("second step");

        try {
            if (Math.random() > 0.5) {
                packInArray(25);
            } else {
                Object object = null;
                System.out.println(object.toString());
            }
        } catch (ArrayIndexOutOfBoundsException aioobe) {
            System.out.println(aioobe.getClass().getName() + " Message: " + aioobe.getMessage());
        } catch (NullPointerException ne) {
            System.out.println(ne.getClass().getName() + " Message: " + ne.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getClass().getName() + " Message: " + ex.getMessage());
        }
    }

    public static void main(String[] args) {
        System.out.println("start program");
        firstStep();
        secondStep();
        System.out.println("end program");
    }

}
