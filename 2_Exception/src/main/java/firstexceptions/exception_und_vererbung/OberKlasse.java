package firstexceptions.exception_und_vererbung;

public class OberKlasse implements InterfaceKlasse {

    @Override
    public void methode1(boolean shouldThrowException) throws UnterException {
        if (shouldThrowException) {
            throw new UnterException(getClass().getName() + " Exception in methode1");
        }
    }

    @Override
    public void methode2(boolean shouldThrowException) throws UnterException {
        if (shouldThrowException) {
            throw new UnterException(getClass().getName() + " Exception in methode2");
        }
    }

    @Override
    public void methode3(boolean shouldThrowException) throws OberException {
        if (shouldThrowException) {
            throw new OberException(getClass().getName() + " Exception in methode3");
        }
    }

    @Override
    public void methode4(boolean shouldThrowException) throws UnterException{
        if (shouldThrowException) {
            throw new UnterException(getClass().getName() + " Exception in methode4");
        }
    }
}
