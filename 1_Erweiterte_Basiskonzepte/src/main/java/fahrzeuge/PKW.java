package fahrzeuge;

public class PKW extends Fahrzeug implements HatMotor {

    private static int serienummerStand = 1;
    public boolean licht = false;
    public int serienummer;

    public PKW() {
        System.out.println("DefaultKonstruktor von: " + PKW.class + " aufgerufen");
        this.serienummer = PKW.serienummerStand++;
        System.out.println("Seriennummer: " + this.serienummer);
    }

    public PKW(String farbe) {
        this();
        System.out.println("Konstruktor von: " + PKW.class + " mit farbe: " + farbe + " aufgerufen");
        this.farbe = farbe;
    }

    @Override
    public void fahren() {
        System.out.println("Der " + farbe + "e PKW fährt");
    }

    @Override
    public void betaetigeGaspedal() {
        fahren(); // oder this.fahren();
    }

    @Override
    public void fuehreServiceDurch() {
        System.out.println("Dauert 1 Tage");
    }

    public void lichtAn() {
        System.out.println("Standart Lampen an");
        this.licht = true;
    }
}




