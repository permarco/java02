package firstexceptions.exception_und_vererbung;

public class UnterKlasse extends OberKlasse {

    /**
     * Eine Unterklasse kann eine Super-Methode mit der gleichen Exception überschreiben.
     *
     * @param shouldThrowException
     * @throws OberException
     */
    @Override
    public void methode1(boolean shouldThrowException) throws UnterException {
        if (shouldThrowException) {
            throw new UnterException(getClass().getName() + " Exception in methode1");
        }
    }

    /**
     * Eine Unterklass kann eine Super-Methode überschreiben und die trhows-Klausel weglassen.
     * Die neue Methode darf aber keiene Exception mehr werfen.
     *
     * @param shouldThrowException
     */
    @Override
    public void methode2(boolean shouldThrowException) {
        if (shouldThrowException) {
//            throw new UnterException();
        }
    }

    /**
     * Eine Methoden in der Unterklasse kann die throw-Klausel einengen (narrowing)
     *
     * @param shouldThrowException
     */
    @Override
    public void methode3(boolean shouldThrowException) throws UnterException {
        if (shouldThrowException) {
            throw new UnterException(getClass().getName() + " Exception in methode3");
        }
    }

    /**
     * Eine Methode in der Unterklasse kann nicht die throws-Klausel erweitern.
     * Weder mit zusätzlichen Exceptions, noch mit hiearchisch allgemeinere Exceptions (widening).
     * Nur die gleiche Klasse oder mit hierarchisch restrikiveren Klassen (narrowing) ist erlaubt.
     *
     * @param shouldThrowException
     */
    @Override
    public void methode4(boolean shouldThrowException) throws UnterException { //OberException, IOException {
        if (shouldThrowException) {
            throw new UnterException(getClass().getName() + " Exception in methode4");
//            throw new OberException(); // nicht möglich Exceptions widening
//            throw new IOException(); // nicht möglich: Exceptionliste erweitern
        }
    }
}
