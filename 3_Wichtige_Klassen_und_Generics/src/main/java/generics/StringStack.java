package generics;

public class StringStack {

    private String[] stack;
    private int pos;


    StringStack(int size) {
        stack = new String[size];
        pos = 0;
    }

    public void push(String word) {
        stack[pos++] = word;
    }

    public String pop() {
        return stack[--pos];
    }

    @Override
    public String toString() {
        String output = "";
        for (int i = 0; stack.length > i; i++) {
            output += "[" + stack[i] + "]";
        }
        return output;
    }
}
