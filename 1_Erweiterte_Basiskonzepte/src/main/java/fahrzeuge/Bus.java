package fahrzeuge;

public class Bus extends Fahrzeug implements HatMotor {

    @Override
    public void fahren() {
        System.out.println("Der " + farbe + "e Bus rollt");
    }

    @Override
    public void betaetigeGaspedal() {
        fahren();  // oder this.fahren();
    }

    @Override
    public void fuehreServiceDurch() {
        System.out.println("Dauert 1 Tag");
    }
}
